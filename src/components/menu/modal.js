import Modal from '@material-ui/core/Modal';
import { withStyles } from '@material-ui/styles';
import PropTypes from 'prop-types';
import React from 'react';
import update from 'react-addons-update';
import { connect } from 'react-redux';
import { compose } from 'redux';

const styles = theme => ({
  button: {
    position: 'absolute',
    top: '10px',
    right: '50px',
    width: '150px',
    height: '40px',
    borderRadius: '5px',
    backgroundColor: 'orange',
    color: '#fff',
    fontSize: '14px',
    outlineWidth: 0,
    cursor: 'pointer  '
  },
  plus: {
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: '18px'
  },
  paper: {
    position: 'absolute',
    width: 800,
    height: 400,
    marginLeft: '-400px',
    marginTop: '-200px',
    outlineWidth: 0,
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #eee',
    boxShadow: theme.shadows[5],
    borderRadius: 10,
    padding: 0,
    overflow: 'hidden'
  },
  header: {
    height: '100px',
    backgroundColor: '#eee',
    '& h2': {
      textAlign: 'left',
      paddingTop: 28,
      marginLeft: 150
    },
    '& p': {
      textAlign: 'left',
      marginLeft: 150
    }
  },
  dishName: {
    marginTop: 20,
    marginLeft: 150,
    '& input': {
      width: 480,
      height: 20,
      border: '1px solid #ddd',
      borderRadius: 5,
      padding: '2px 10px'
    }
  },
  dishDescription: {
    marginTop: 5,
    marginLeft: 150,
    '& textarea': {
      width: 480,
      border: '1px solid #ddd',
      borderRadius: 5,
      padding: '2px 10px'
    }
  },
  ingredients: {
    position: 'relative',
    marginTop: 5,
    marginLeft: 150,
    width: 500,
    '& h4': {},
    '& a': {
      position: 'absolute',
      right: 0,
      top: 0,
      color: 'orange',
      cursor: 'pointer'
    },
    '& a span': {
      fontWeight: 'bold'
    }
  },
  ingredientsList: {
    marginTop: 5,
    marginBottom: 10,
    marginLeft: 150,
    width: 500,
    '& input.name': {
      width: 400,
      marginRight: 2,
      border: '1px solid #ddd',
      borderRadius: 2
    },
    '& input.weight': {
      width: 65,
      marginRight: 2,
      border: '1px solid #ddd',
      borderRadius: 2
    }
  },
  submit: {
    marginTop: 5,
    marginLeft: 150,
    width: 500,
    position: 'relative',
    '& button': {
      position: 'absolute',
      right: 0,
      top: 0,
      width: 150,
      cursor: 'pointer',
      height: 40,
      borderRadius: 5,
      outlineWidth: 0,
      backgroundColor: 'orange',
      color: '#fff',
      fontSize: 14
    }
  }
});

class ModalComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      ingredients: [{ title: 'meat', weight: '100' }]
    };
    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  handleOpen() {
    this.setState({ showModal: true });
  }

  handleClose() {
    this.setState({ showModal: false });
  }

  render() {
    const { classes } = this.props;
    const fields = this.state.ingredients.map((item, index) => {
      return (
        <p>
          <input
            type="text"
            className="name"
            placeholder="Ingredient name"
            value={item.title}
            onChange={e => {
              let updater = {};
              updater[index] = { title: { $set: e.target.value } };
              this.setState({
                ingredients: update(this.state.ingredients, updater)
              });
            }}
          />
          <input
            type="text"
            className="weight"
            placeholder="Weight (Kcl)"
            value={item.weight}
            onChange={e => {
              let updater = {};
              updater[index] = { weight: { $set: e.target.value } };
              this.setState({
                ingredients: update(this.state.ingredients, updater)
              });
            }}
          />
          <button
            onClick={() => {
              this.setState({
                ingredients: this.state.ingredients.filter(
                  (_, i) => i !== index
                )
              });
            }}
          >
            -
          </button>
        </p>
      );
    });
    return (
      <>
        <button className={classes.button} onClick={this.handleOpen}>
          Add a new dish <i className={classes.plus}>+</i>
        </button>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.showModal}
          onClose={this.handleClose}
        >
          <div className={classes.paper} style={{ top: '50%', left: '50%' }}>
            <div className={classes.header}>
              <h2 id="simple-modal-title">Add a new dish</h2>
              <p id="simple-modal-description">
                Please enter all informations about your new dish
              </p>
            </div>
            <div>
              {/*}<p>Dish title:</p>{*/}
              <p className={classes.dishName}>
                <input
                  type="text"
                  placeholder="Dish Name"
                  value={this.props.newDishTitle}
                  onChange={e => {
                    console.log(e.target.value);
                    this.props.setDishTitle(e.target.value);
                  }}
                />
              </p>
              {/*}<p>Dish description:</p>{*/}
              <p className={classes.dishDescription}>
                <textarea
                  cols="40"
                  rows="5"
                  placeholder="Dish Description"
                  value={this.props.newDishDescr}
                  onChange={e => {
                    console.log(e.target.value);
                    this.props.setDishDescr(e.target.value);
                  }}
                />
              </p>
              <p className={classes.ingredients}>
                <h4>Ingredients</h4>
                <a
                  onClick={() => {
                    if (this.state.ingredients.length < 4) {
                      this.setState({
                        ingredients: [
                          ...this.state.ingredients,
                          ...[{ title: 'meat', weight: '100' }]
                        ]
                      });
                    }
                  }}
                >
                  Add a new ingredient <span>+</span>
                </a>
              </p>
              <p className={classes.ingredientsList}>{fields}</p>
              <p className={classes.submit}>
                <button
                  onClick={() => {
                    this.props.addNewDish(this.state.ingredients);
                    this.setState({ showModal: false });
                    setTimeout(() => {
                      this.props.setDishDescr('');
                      this.props.setDishTitle('');
                      this.setState({
                        ingredients: [{ title: 'meat', weight: '100' }]
                      });
                    }, 500);
                  }}
                >
                  Add a new Dish
                </button>
              </p>
            </div>
          </div>
        </Modal>
      </>
    );
  }
}

const mapStateToProps = ({ count, title, newDishTitle, newDishDescr }) => {
  return { count, title, newDishTitle, newDishDescr };
};

const mapDispatchToProps = dispatch => {
  return {
    multiply: () => dispatch({ type: `MULTIPLY` }),
    setDishTitle: data =>
      dispatch({
        type: `SET_DISH_TITLE`,
        data
      }),
    setDishDescr: data =>
      dispatch({
        type: `SET_DISH_DESCR`,
        data
      }),
    addNewDish: data =>
      dispatch({
        type: `ADD_NEW_DISH`,
        data
      })
  };
};

ModalComponent.propTypes = {
  classes: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  newDishTitle: PropTypes.string.isRequired,
  newDishDescr: PropTypes.string.isRequired,
  count: PropTypes.number.isRequired,
  multiply: PropTypes.func.isRequired,
  setDishTitle: PropTypes.func.isRequired,
  setDishDescr: PropTypes.func.isRequired,
  setDishWeight: PropTypes.func.isRequired,
  addNewDish: PropTypes.func.isRequired
};

export default compose(
  withStyles(styles, {
    name: 'TestComponent'
  }),
  connect(mapStateToProps, mapDispatchToProps)
)(ModalComponent);
