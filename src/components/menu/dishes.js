import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/styles';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import styled from 'styled-components';
import loader from '../../images/menu/loader.gif';

const styles = theme => ({
  cardroot: {
    maxWidth: 345
  },
  cardmedia: {
    height: 140
  },
  root: {
    padding: '0px',
    backgroundImage: `linear-gradient(180deg, #F4F3FD 0%, #DDEFFF 93.69%)`,
    [theme.breakpoints.up('sm')]: {
      paddingBottom: '0px'
    },
    [theme.breakpoints.up('md')]: {
      padding: '0px'
    }
  },
  card: {
    display: 'flex',
    height: '100%',
    minHeight: 180,
    padding: '20px',
    [theme.breakpoints.up('sm')]: {
      minHeight: 160
    },
    [theme.breakpoints.up('md')]: {
      padding: '0px'
    },
    [theme.breakpoints.up('lg')]: {
      paddingRight: '0px'
    }
  },
  bottom: {
    position: 'relative',
    width: '100%',
    height: '200px'
  },
  loader: {
    position: 'absolute',
    top: '0',
    left: '50%',
    width: '100px',
    height: '100px',
    marginLeft: '-50px'
  },
  hidedLoader: {
    display: 'none'
  }
});

const SearchText = styled.div`
  s {
    background-color: yellow;
    color: #000;
    border-radius: 3px;
    outline: 1px solid #fcdb9f;
    text-decoration: none;
  }
`;
class Dishes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      maxDishes: 6000
    };
  }

  componentDidMount() {
    if (typeof window !== `undefined`) {
      window.addEventListener('scroll', () => {
        if (
          document.documentElement.clientHeight + window.scrollY ==
          document.documentElement.scrollHeight
        ) {
          if (
            !this.props.title &&
            this.props.dishes.length < this.state.maxDishes
          )
            this.props.multiply();
        }
      });
    }
  }

  displayText(text) {
    return this.props.title
      ? text.replace(new RegExp(this.props.title, 'ig'), `<s>$&</s>`)
      : text;
  }

  render() {
    // console.log('[STATE]', this.state);
    const { classes } = this.props;
    const items = this.props.filteredDishes.map(item => (
      <Grid item xs={4} sm={4} id="animatePlatform" className="animateP d2p">
        <Card className={classes.cardroot}>
          <CardActionArea>
            <CardMedia
              className={classes.cardmedia}
              image={item.img}
              title={item.title}
            />
            <CardContent>
              <Typography
                gutterBottom
                variant="h5"
                component="h2"
                dangerouslySetInnerHTML={{
                  __html: this.displayText(item.title)
                }}
              />
              <Typography
                variant="body2"
                color="textSecondary"
                component="p"
                dangerouslySetInnerHTML={{
                  __html: this.displayText(item.descr)
                }}
              />
            </CardContent>
          </CardActionArea>
          <CardActions>
            <span style={{ display: 'inline-block', marginLeft: '10px' }}>
              {item.totalWeight}gr
            </span>
            <Button size="small" color="primary">
              Details
            </Button>
          </CardActions>
        </Card>
      </Grid>
    ));
    return (
      <SearchText>
        <Container maxWidth="xl">
          <Grid container spacing={10}>
            {items}
          </Grid>
          <div className={classes.bottom}>
            <img
              className={
                this.props.title ||
                this.props.dishes.length >= this.state.maxDishes
                  ? classes.hidedLoader
                  : classes.loader
              }
              src={loader}
            />
          </div>
        </Container>
      </SearchText>
    );
  }
}

const mapStateToProps = ({ count, title, dishes }) => {
  return {
    count,
    title,
    dishes,
    filteredDishes: dishes.filter(item =>
      item.title.match(new RegExp(title, 'ig')) ||
      item.descr.match(new RegExp(title, 'ig'))
        ? item
        : null
    )
  };
};

const mapDispatchToProps = dispatch => {
  return { multiply: () => dispatch({ type: `MULTIPLY_DISHES` }) };
};

Dishes.propTypes = {
  dishes: PropTypes.array.isRequired,
  filteredDishes: PropTypes.array.isRequired,
  classes: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  count: PropTypes.number.isRequired,
  multiply: PropTypes.func.isRequired
};

export default compose(
  withStyles(styles, {
    name: 'TestComponent'
  }),
  connect(mapStateToProps, mapDispatchToProps)
)(Dishes);
