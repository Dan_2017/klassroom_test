import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

const Counter = ({ title, count, increment, setTitle }) => (
  <div>
    <p> Count: {count} </p> <button onClick={increment}> Increment </button>{' '}
    <br />
    <input
      type="text"
      onChange={e => {
        // console.log(e.target.value);
        setTitle(e.target.value);
      }}
      value={title}
    />
  </div>
);

Counter.propTypes = {
  title: PropTypes.string.isRequired,
  count: PropTypes.number.isRequired,
  increment: PropTypes.func.isRequired,
  setTitle: PropTypes.func.isRequired
};

const mapStateToProps = ({ title, count }) => {
  return {
    title,
    count
  };
};

const mapDispatchToProps = dispatch => {
  return {
    increment: () =>
      dispatch({
        type: `INCREMENT`
      }),
    setTitle: data =>
      dispatch({
        type: `SET_TITLE`,
        data
      })
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Counter);
