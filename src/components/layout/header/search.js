import { makeStyles } from '@material-ui/styles';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

const useStyles = makeStyles(theme => ({
  search: {
    display: 'inline-block',
    verticalAlign: 'top',
    margin: '23px'
  },
  input: {
    width: '170px',
    padding: '7px 15px',
    border: '0px solid #fff',
    borderRadius: '5px',
    outlineWidth: 0
  }
}));

const Search = ({ title, setTitle }) => {
  const classes = useStyles();
  return (
    <div className={classes.search}>
      <input
        className={classes.input}
        type="text"
        placeholder="Try «Chicken cotoletta»"
        onChange={e => {
          setTitle(e.target.value);
        }}
        value={title}
      />
    </div>
  );
};

Search.propTypes = {
  title: PropTypes.string.isRequired,
  setTitle: PropTypes.func.isRequired
};

const mapStateToProps = ({ title }) => {
  return {
    title
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setTitle: data =>
      dispatch({
        type: `SET_TITLE`,
        data
      })
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Search);
