import { makeStyles } from '@material-ui/styles';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

const useStyles = makeStyles(theme => ({
  title: {
    color: '#fff',
    margin: '30px',
    marginLeft: '60px'
  }
}));

const PageTitle = ({ pageTitle }) => {
  const classes = useStyles();
  return <h1 className={classes.title}>{pageTitle}</h1>;
};

PageTitle.propTypes = {
  pageTitle: PropTypes.string.isRequired
};

const mapStateToProps = ({ pageTitle }) => {
  return {
    pageTitle
  };
};

export default connect(mapStateToProps)(PageTitle);
