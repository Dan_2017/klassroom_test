import { makeStyles } from '@material-ui/styles';
import { Link } from 'gatsby';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

const useStyles = makeStyles(theme => ({
  ul: {
    display: 'inline-block',
    verticalAlign: 'top',
    backgroundSize: 'cover',
    width: '45%',
    height: '50px',

    '& li': {
      display: 'inline-block',
      padding: '10px',
      '& a': {
        padding: '10px',
        borderRadius: '5px',
        color: '#fff',
        textDecoration: 'none'
      }
    }
  },
  item: {},
  activeItem: { backgroundColor: 'rgba(245, 132, 0, .9)' }
}));

const Navigation = ({ navigation, setPage }) => {
  const classes = useStyles();
  const setActivePage = e => {
    setPage(e.target.getAttribute('data-id'));
  };
  const items = Object.entries(navigation).map(item => {
    if (item[1].title === 'Home') return;
    else
      return (
        <li key={item[1].link}>
          <Link
            to={'/' + item[1].link + '/'}
            onClick={setActivePage}
            data-id={item[1].title}
            className={item[1].isActive ? classes.activeItem : classes.item}
          >
            {item[1].title}
          </Link>
        </li>
      );
  });
  return <ul className={classes.ul}>{items}</ul>;
};

Navigation.propTypes = {
  navigation: PropTypes.object,
  setPage: PropTypes.func.isRequired
};

const mapStateToProps = ({ navigation }) => {
  return {
    navigation
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setPage: data =>
      dispatch({
        type: `SET_PAGE`,
        data
      })
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Navigation);
