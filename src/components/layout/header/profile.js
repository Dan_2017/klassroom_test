import { makeStyles } from '@material-ui/styles';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import profile from '../../../images/header/profile.png';

const useStyles = makeStyles(theme => ({
  profile: {
    display: 'inline-block',
    verticalAlign: 'top',
    margin: '20px'
  },
  profile_name: {
    display: 'inline-block',
    color: '#fff',
    height: '20px',
    marginRight: '10px'
  },
  profile_logo: {
    display: 'inline-block',
    backgroundImage: `url(${profile})`,
    backgroundSize: 'cover',
    width: '18px',
    height: '20px',
    transform: 'translateY(4px)'
  }
}));

const Profile = ({ profile }) => {
  const classes = useStyles();

  return (
    <div className={classes.profile}>
      <span className={classes.profile_name}> {profile} </span>
      <span className={classes.profile_logo} />
    </div>
  );
};

Profile.propTypes = {
  profile: PropTypes.string.isRequired
};

const mapStateToProps = ({ profile }) => {
  return {
    profile
  };
};

export default connect(mapStateToProps)(Profile);
