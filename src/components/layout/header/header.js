import { makeStyles } from '@material-ui/styles';
import { Link } from 'gatsby';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import bg from '../../../images/header/header.png';
import logo from '../../../images/header/logo-white.png';
import Navigation from './navigation';
import PageTitle from './page-title';
import Profile from './profile';
import Search from './search';

const useStyles = makeStyles(theme => ({
  header: {
    backgroundImage: `url(${bg})`,
    backgroundSize: 'cover',
    width: '100%',
    height: '250px'
  },
  logo: {
    display: 'inline-block',
    margin: '10px',
    marginLeft: '60px',
    backgroundImage: `url(${logo})`,
    backgroundSize: 'cover',
    width: '69px',
    height: '43px'
  }
}));

const Header = ({ setHomePage }) => {
  const classes = useStyles();

  return (
    <>
      <div className={classes.header}>
        <Link
          to="/"
          title="Dish"
          className={classes.logo}
          onClick={setHomePage}
        ></Link>
        <Navigation />
        <Search />
        <Profile />
        <PageTitle />
      </div>
    </>
  );
};

Header.propTypes = {
  navigation: PropTypes.object,
  setHomePage: PropTypes.func.isRequired
};

const mapStateToProps = ({ navigation }) => {
  return {
    navigation
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setHomePage: data =>
      dispatch({
        type: `SET_PAGE`,
        data: 'Home'
      })
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
