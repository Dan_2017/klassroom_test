import { makeStyles } from '@material-ui/styles';
import PropTypes from 'prop-types';
import React from 'react';

const useStyles = makeStyles(theme => ({
  container: {
    position: 'relative',
    margin: '20px'
  },
  title: {
    color: '#000',
    margin: '30px',
    height: '40px',
    borderLeft: '7px solid orange',
    paddingLeft: '7px',
    marginBottom: '0px',
    fontSize: '24px',
    lineHeight: '17px'
  },
  description: {
    color: '#000',
    margin: '30px',
    marginTop: '-10px',
    paddingLeft: '15px'
  }
}));

const PageTitle2 = props => {
  // eslint-disable-next-line
  const { children, title, description, ...other } = props;
  const classes = useStyles();
  return (
    <div className={classes.container}>
      <h3 className={classes.title}>{title}</h3>
      <p className={classes.description}>{description}</p>
      {children}
    </div>
  );
};

PageTitle2.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string
};

PageTitle2.defaultProps = {
  title: 'Page',
  description: 'Description'
};

export default PageTitle2;
