import { createMuiTheme } from '@material-ui/core';
import merge from 'lodash/merge';

import breakpoints from './breakpoints';
import colors from './colors';
import mixins from './mixins';
import createOverrides from './overrides';
import palette from './palette';
import props from './props';
import shadows from './shadows';
import shape from './shape';
import typography from './typography';

const theme = createMuiTheme({
  colors,
  breakpoints,
  palette,
  typography,
  shadows,
  props,
  shape,
  mixins,
  spacing: 4
});

theme.overrides = merge(theme.overrides, createOverrides(theme));

export default theme;
