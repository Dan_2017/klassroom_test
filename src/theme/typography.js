export default {
  fontFamily: ['Open Sans', 'Roboto', 'sans-serif'].join(','),
  htmlFontSize: 16,
  fontSize: 14,
  h1: {
    margin: 0,
    fontSize: '2.25rem', // 36px
    fontWeight: 400,
    lineHeight: 1.5,
    letterSpacing: 1
  },
  h2: {
    margin: 0,
    fontSize: '2rem', // 32px
    fontWeight: 700,
    lineHeight: 1.6,
    letterSpacing: 1
  },
  h3: {
    margin: 0,
    fontSize: '2rem', // 32px
    fontWeight: 400,
    lineHeight: 1,
    letterSpacing: 0
  },
  h4: {
    margin: 0,
    fontSize: '1.5rem', // 24px
    fontWeight: 600,
    lineHeight: 1.3,
    letterSpacing: 0
  },
  h5: {
    margin: 0,
    fontSize: '1rem', // 16px
    fontWeight: 600,
    lineHeight: 1.2,
    letterSpacing: 0
  },
  h6: {
    margin: 0,
    fontSize: '0.75rem', // 12px
    fontWeight: 700,
    lineHeight: 1.2,
    letterSpacing: 0
  },
  body1: {
    fontSize: '1rem', // 16px
    lineHeight: 1.9
  },
  button: {
    lineHeight: 1.357,
    borderRadius: 4,
    outline: 'none',
    cursor: 'pointer'
  }
};
