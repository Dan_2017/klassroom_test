import colors from '../colors';
import shape from '../shape';

export default {
  root: {
    border: `1px solid ${colors.borderColor}`,
    borderRadius: shape.borderRadius
  }
};
