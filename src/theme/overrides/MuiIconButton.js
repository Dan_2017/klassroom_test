import { fade } from '@material-ui/core/styles/colorManipulator';

import colors from '../colors';
import palette from '../palette';
import shape from '../shape';

export default {
  root: {
    padding: 6,
    color: palette.primary.main,
    borderRadius: shape.borderRadius,
    '&:hover': {
      backgroundColor: fade(palette.primary.main, 0.1)
    },
    '&:active': {
      background: fade(palette.primary.main, 0.7),
      '& .MuiSvgIcon-root': {
        fill: colors.white
      }
    },
    '&.Mui-disabled': {
      opacity: '0.5'
    }
  }
};
