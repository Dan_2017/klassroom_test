import MuiBackdrop from './MuiBackdrop';
import MuiButton from './MuiButton';
import MuiContainer from './MuiContainer';
import MuiIconButton from './MuiIconButton';
import MuiInputBase from './MuiInputBase';
import MuiInputLabel from './MuiInputLabel';
import MuiList from './MuiList';
import MuiListItem from './MuiListItem';
import MuiOutlinedInput from './MuiOutlinedInput';
import MuiTypography from './MuiTypography';

const createOverrides = theme => ({
  MuiTypography: MuiTypography(theme),
  MuiButton: MuiButton(theme),
  MuiIconButton,
  MuiInputBase,
  MuiOutlinedInput: MuiOutlinedInput(theme),
  MuiInputLabel,
  MuiList,
  MuiListItem,
  MuiContainer: MuiContainer(theme),
  MuiBackdrop
});

export default createOverrides;
