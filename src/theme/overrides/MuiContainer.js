export default theme => ({
  root: {
    paddingRight: 24,
    paddingLeft: 24,
    [theme.breakpoints.up('sm')]: {
      paddingRight: 36,
      paddingLeft: 36
    },
    [theme.breakpoints.up('md')]: {
      paddingRight: 48,
      paddingLeft: 48
    },
    [theme.breakpoints.up('lg')]: {
      paddingRight: 92,
      paddingLeft: 92
    }
  }
});
