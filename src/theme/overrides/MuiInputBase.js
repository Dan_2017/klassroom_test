import colors from '../colors';
import palette from '../palette';

export default {
  root: {
    background: colors.white,
    '&.Mui-disabled': {
      background: palette.action.disabledBackground
    },
    '&:hover:not(.Mui-focused)': {
      color: palette.black
    }
  },
  input: {
    fontSize: 14,
    lineHeight: 1.357,
    '&::placeholder': {
      fontWeight: 400,
      color: palette.text.primary,
      opacity: 1
    }
  }
};
