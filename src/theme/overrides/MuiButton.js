import { darken, fade } from '@material-ui/core/styles/colorManipulator';

import colors from '../colors';
import palette from '../palette';

export default theme => ({
  root: {
    padding: theme.spacing(3.5, 8),
    fontWeight: 600,
    textTransform: 'none',
    color: palette.text.secondary,

    '&.Mui-disabled': {
      opacity: '.5'
    }
  },
  contained: {
    color: palette.text.primary,
    boxShadow: 'none',
    borderRadius: 4,
    '&:hover': {
      backgroundColor: darken(palette.grey[300], 0.045),
      boxShadow: 'none'
    },
    '&:active': {
      backgroundColor: darken(palette.grey[300], 0.14),
      boxShadow: 'none'
    }
  },
  containedPrimary: {
    '&:active': {
      backgroundColor: colors.darkPrimaryColor,
      boxShadow: 'none'
    },
    '&.Mui-disabled': {
      color: colors.white,
      backgroundColor: palette.primary.main
    }
  },
  outlined: {
    padding: theme.spacing(3.5, 8),
    borderRadius: 3
  },
  outlinedPrimary: {
    border: `2px solid ${palette.primary.main}`,
    '&:hover': {
      borderWidth: 2
    },
    '&:active': {
      color: colors.white,
      background: fade(palette.primary.main, 0.7),
      '& .MuiSvgIcon-root': {
        fill: colors.white
      }
    },
    '&.Mui-disabled': {
      color: palette.primary.main,
      borderColor: palette.primary.main,
      borderWidth: 2
    }
  }
});
