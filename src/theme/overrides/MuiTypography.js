export default theme => ({
  body1: {
    [theme.breakpoints.up('md')]: {
      fontSize: '1.25rem', // 20px
      lineHeight: 2.22
    }
  },
  h1: {
    [theme.breakpoints.up('md')]: {
      fontSize: '3rem' // 48px
    }
  }
});
