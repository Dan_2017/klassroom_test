import { fade } from '@material-ui/core/styles/colorManipulator';

import colors from '../colors';

export default {
  root: {
    backgroundColor: fade(colors.white, 0.92)
  }
};
