import { ThemeProvider } from '@material-ui/core/styles';
import { makeStyles } from '@material-ui/styles';
import { globalHistory } from '@reach/router';
import { graphql, useStaticQuery } from 'gatsby';
import PropTypes from 'prop-types';
import React from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import Header from '../../components/layout/header/header';
import favicon from '../../images/favicon.ico';
import theme from '../../theme/index';

const useStyles = makeStyles(theme => ({
  '@global': {
    body: {
      height: '100%',
      backgroundColor: '#eee'
    },
    html: {
      height: '100%'
    },
    '#___gatsby': {
      height: '100%'
    },
    'h1, h2, h3, h4, h5, h6': {
      margin: 0
    },
    p: {
      margin: 0
    },
    '#apps-block>.MuiGrid-spacing-xs-3': {
      width: '100%'
    }
  },
  root: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh'
  },
  main: {
    flex: '1 0 auto',
    overflow: 'hidden',
    backgroundColor: '#fff'
  }
}));

const DefaultLayout = ({ children, setPageOnLoad }) => {
  const metadata = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `);

  React.useEffect(() => {
    const currentPath = globalHistory.location.pathname;
    switch (currentPath) {
      case '/about/':
        setPageOnLoad('Our Restaurant');
        break;
      case '/menu/':
        setPageOnLoad('Menu');
        break;
      case '/contact/':
        setPageOnLoad('Contacts');
        break;
    }
  }, [setPageOnLoad]);

  const classes = useStyles();

  return (
    <>
      <Helmet>
        <title> {metadata.site.siteMetadata.title} </title>
        <link rel="icon" href={favicon} />
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800|Roboto:300,400,500&display=swap"
        />
      </Helmet>
      <ThemeProvider theme={theme}>
        <header>
          <Header />
        </header>
        <main className={classes.main}>{children}</main>
        <footer></footer>
      </ThemeProvider>
    </>
  );
};

DefaultLayout.propTypes = {
  setPageOnLoad: PropTypes.func.isRequired
};

const mapDispatchToProps = dispatch => {
  return {
    setPageOnLoad: data =>
      dispatch({
        type: 'SET_PAGE',
        data
      })
  };
};

export default connect(null, mapDispatchToProps)(DefaultLayout);
