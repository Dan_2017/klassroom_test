import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import Title from '../components/layout/main/title.js';
import Dishes from '../components/menu/dishes';
import Modal from '../components/menu/modal';
import Layout from '../containers/layout';

const Menu = () => (
  <Layout>
    <Title
      title={'Meat Dishes'}
      description={'Some of the best meat dishes from worldwide'}
    >
      <Modal />
    </Title>
    <Dishes />
  </Layout>
);

Menu.propTypes = {
  count: PropTypes.number.isRequired,
  multiply: PropTypes.func.isRequired
};

const mapStateToProps = ({ count }) => {
  return {
    count
  };
};

const mapDispatchToProps = dispatch => {
  return {
    multiply: () =>
      dispatch({
        type: `MULTIPLY`
      })
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Menu);
