import React from 'react';
import Title from '../components/layout/main/title.js';
import Layout from '../containers/layout';

const About = () => (
  <Layout>
    <Title
      title={'Our restaurant'}
      description={'Lorem ipsum dolor sit amet.'}
    />
  </Layout>
);

export default About;
