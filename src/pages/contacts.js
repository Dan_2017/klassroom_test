import React from 'react';
import Title from '../components/layout/main/title.js';
import Layout from '../containers/layout';

const Contacts = () => (
  <Layout>
    <Title title={'Contacts'} description={'Lorem ipsum dolor sit amet.'} />
  </Layout>
);

export default Contacts;
