import React from 'react';
import Title from '../components/layout/main/title.js';
import Layout from '../containers/layout';


const Home = () => (
  <Layout>
    <Title
      title={'Home'}
      description={'Lorem ipsum dolor sit amet.'}
    />
  </Layout>
);

export default Home;
