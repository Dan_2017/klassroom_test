import { applyMiddleware, createStore as reduxCreateStore } from 'redux';
import logger from 'redux-logger';

const reducer = (state, action) => {
  if (action.type === `INCREMENT`) {
    return Object.assign({}, state, {
      count: state.count + 1
    });
  }
  if (action.type === `MULTIPLY`) {
    return Object.assign({}, state, {
      count: state.count * state.count
    });
  }
  if (action.type === `SET_TITLE`) {
    return Object.assign({}, state, {
      title: action.data
    });
  }
  if (action.type === `SET_DISH_TITLE`) {
    return Object.assign({}, state, {
      newDishTitle: action.data
    });
  }
  if (action.type === `SET_DISH_DESCR`) {
    return Object.assign({}, state, {
      newDishDescr: action.data
    });
  }
  if (action.type === `SET_DISH_WEIGHT`) {
    return Object.assign({}, state, {
      newDishTotalWeight: action.data
    });
  }
  if (action.type === `SET_PAGE`) {
    let navigation = Object.assign({}, state.navigation);
    let pageTitle = '';
    for (let i in navigation) {
      if (i === action.data) {
        navigation[i].isActive = true;
        pageTitle = navigation[i].pageTitle;
      } else {
        navigation[i].isActive = false;
      }
    }
    return Object.assign({}, state, {
      navigation,
      pageTitle
    });
  }
  if (action.type === `MULTIPLY_DISHES`) {
    return Object.assign({}, state, {
      dishes: [...state.dishes, ...state.dishes.slice(0, 6)]
    });
  }
  if (action.type === `ADD_NEW_DISH`) {
    console.log(action.data);
    return Object.assign({}, state, {
      dishes: [
        ...state.dishes,
        {
          img: '/images/cards/Dish_1.png',
          title: state.newDishTitle,
          descr: state.newDishDescr,
          ingredients: action.data,
          totalWeight: action.data.reduce(
            (sum, item) => sum + parseInt(item.weight),
            0
          )
        }
      ]
    });
  }
  return state;
};

const initialState = {
  count: 0,
  newDishTitle: '',
  newDishDescr: '',
  profile: 'John C.',
  title: '',
  pageTitle: 'Welcome',
  navigation: {
    Home: {
      title: 'Home',
      link: '',
      pageTitle: 'Welcome',
      isActive: false
    },
    'Our Restaurant': {
      title: 'Our Restaurant',
      link: 'about',
      pageTitle: 'Our Restaurant',
      isActive: false
    },
    Menu: { title: 'Menu', link: 'menu', pageTitle: 'Menu', isActive: false },
    Contacts: {
      title: 'Contacts',
      link: 'contacts',
      pageTitle: 'Contacts',
      isActive: false
    }
  },
  dishes: [
    {
      img: '/images/cards/Dish_1.png',
      title: 'Roasted Butternut Pumpkin, Shiitake Mushroom and Haloumi Salad',
      descr:
        'A hearty mix of fresh greens, roasted vegetables and golden haloumi makes up this tasty winter salad.',
      ingredients: [{ title: 'meat', weight: '100' }],
      totalWeight: 100
    },
    {
      img: '/images/cards/Dish_2.png',
      title: 'Slow-cooked, Italian Beef Cheek Ragú with Pappardelle',
      descr:
        'Slow-cooked beef cheek ragù. Serve with just-cooked pappardelle and sprinkle with lashings of Parmesan, of course.',
      ingredients: [{ title: 'meat', weight: '100' }],
      totalWeight: 100
    },
    {
      img: '/images/cards/Dish_3.png',
      title:
        'Chicken Cotoletta with Brussels Sprouts, Rocket and Hazelnut Salad',
      descr: 'The super-crispy outer also happens to be gluten free.',
      ingredients: [{ title: 'meat', weight: '100' }],
      totalWeight: 100
    },
    {
      img: '/images/cards/Dish_4.png',
      title: 'Roasted Butternut Pumpkin, Shiitake Mushroom and Haloumi Salad',
      descr:
        'A hearty mix of fresh greens, roasted vegetables and golden haloumi makes up this tasty winter salad.',
      ingredients: [{ title: 'meat', weight: '100' }],
      totalWeight: 100
    },
    {
      img: '/images/cards/Dish_5.png',
      title: 'Slow-cooked, Italian Beef Cheek Ragú with Pappardelle',
      descr:
        'Slow-cooked beef cheek ragù. Serve with just-cooked pappardelle and sprinkle with lashings of Parmesan, of course.',
      ingredients: [{ title: 'meat', weight: '100' }],
      totalWeight: 100
    },
    {
      img: '/images/cards/Dish_6.png',
      title:
        'Chicken Cotoletta with Brussels Sprouts, Rocket and Hazelnut Salad',
      descr: 'The super-crispy outer also happens to be gluten free.',
      ingredients: [{ title: 'meat', weight: '100' }],
      totalWeight: 100
    }
  ]
};

const createStore = () =>
  reduxCreateStore(reducer, initialState, applyMiddleware(logger));
export default createStore;
