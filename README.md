# Test (React developer / front-end).

You need to make a restaurant page with a list of dishes.

The list of dishes has endless scrolling (when scrolling down the page, the list is duplicated).
Part of the list should be statically stored in the code and displayed when the project starts.
You need to make a form for adding a new dish, in the form of the following fields:

- Name of the dish.
- List of ingredients (name of ingredient, number of grams)
- The total weight of the dish (summed from the number of grams of all ingredients).

The form for adding a dish appears in the form of a modal window (you can use any libraries). At
the top of the screen should be a search field for dish by the name and / or by the ingredient. The
test needs to be done using React, webpack, other libraries you can use which you want.

The job is hosted on GitHub or bitbucket.

Launch instructions should be in the description of the repository.

We are paying attention to the quality of the code and appearance.

Showcase Design & Styleguide :
[https://scene.zeplin.io/project/5d81342145bcf3786e516ae8](https://scene.zeplin.io/project/5d81342145bcf3786e516ae8)

Zeplin App link to “Dish Restaurant” project :
[zpl://project?pid=5d81342145bcf3786e516ae8](zpl://project?pid=5d81342145bcf3786e516ae8)

Sketch file + Fonts :
[zpl://project?pid=5d81342145bcf3786e516ae8](https://drive.google.com/open?id=1nR3GvZEqKfUBA6NauWeZ0xyOL8Tao2Lo)

### Installation

This project uses [GatsbyJS](https://www.gatsbyjs.org/) - modern ReactJS based framework.

To run project locally:

- clone or download it
- install NodeJS and NPM
- install GatsbyJS: `npm i -g gatsby-cli`
- in project folder run:

```
npm i
gatsby develop
```
